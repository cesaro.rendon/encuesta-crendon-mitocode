import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import {Router} from '@angular/router';
import { SecurityService } from 'src/app/_service/security.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private myRoute: Router,
    private ServiceSecurity: SecurityService){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(this.ServiceSecurity.inicioSesion()){
      return true;
    }else{
      window.location.href = 'https://encuesta-crendon-mitocode.auth.us-east-1.amazoncognito.com/login?response_type=token&client_id=1brn6kve37rl4gj6c7je4t721q&redirect_uri=https://d1waxv2fn3nvhb.cloudfront.net/security';    
      return false;
    }
  }
}