import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { VotosComponent } from './_pages/votos/votos.component';
import { VotoComponent } from './_pages/voto/voto.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatButtonModule} from '@angular/material/button';
import { SecurityComponent } from './_pages/security/security.component';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { TokenInterceptorService } from './_service/token-interceptor.service';
import { MenuComponent } from './_pages/menu/menu.component';
import {MatMenuModule} from '@angular/material/menu';
import { InicioComponent } from './_pages/inicio/inicio.component';
import { AuthGuard } from './_shared/guard/auth.guard';



@NgModule({
  declarations: [
    AppComponent,
    VotosComponent,
    VotoComponent,
    SecurityComponent,
    MenuComponent,
    InicioComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,    
    MatTableModule ,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatButtonModule,
    FormsModule,
    MatMenuModule
  ],
  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    { provide: LocationStrategy, useClass: PathLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
 