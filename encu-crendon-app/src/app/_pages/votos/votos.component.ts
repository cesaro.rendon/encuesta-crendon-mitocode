import { Component, OnInit } from '@angular/core';
import { ConsultarVotosService } from 'src/app/_service/consultar-votos.service';
import { Voto } from 'src/app/_model/voto';
import { SecurityService } from 'src/app/_service/security.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-votos',
  templateUrl: './votos.component.html',
  styleUrls: ['./votos.component.css']
})
export class VotosComponent implements OnInit {

  title = 'Resultados Votacion';
  dataSource: Array<Voto>;
  displayedColumns = ["nombres", "apellidos","profesion", "lugarTrabajo","edad", "valorVoto"];
  constructor(private ServiceVoto: ConsultarVotosService,
    private ServiceSecurity: SecurityService,
    private router: Router) {}

  ngOnInit() {
      if (!this.ServiceSecurity.esRoleAdmin()) {
        alert("Error: Acceso restringido");
        this.router.navigate(["/inicio"]);
      } else {
        this.ServiceVoto.consultarAllVotos().subscribe(data => {
          this.dataSource = data;
        })
      }
  }

}
