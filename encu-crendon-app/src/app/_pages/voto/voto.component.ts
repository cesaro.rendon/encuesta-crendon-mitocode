import { Component, OnInit } from '@angular/core';
import { EnviarVotoService } from 'src/app/_service/enviar-voto.service';
import { Voto } from 'src/app/_model/Voto';
import { SecurityService } from 'src/app/_service/security.service';


export interface Lenguaje {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-voto',
  templateUrl: './voto.component.html',
  styleUrls: ['./voto.component.css']
})
export class VotoComponent{


  title = 'Realizar Voto';
  lenguajes: Lenguaje[] = [{ viewValue: 'Java', value: 'Java' }, { viewValue: 'C#', value: 'C#' }]

  votoEnvio = new Voto();

  constructor(private serviceEnviaVoto: EnviarVotoService) {}

  onClickVoto() {
    console.log(JSON.stringify(this.votoEnvio));
    this.serviceEnviaVoto.enviarVoto(this.votoEnvio).subscribe();
    alert("Voto enviado...");
  }


}
