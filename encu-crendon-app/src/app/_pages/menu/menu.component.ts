import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SecurityService } from 'src/app/_service/security.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private router: Router,
    private ServiceSecurity: SecurityService) { }

  ngOnInit() {
  }

  onClickVotar(){
    this.router.navigate(['/voto']);
  }

  onClickResultados(){
    this.router.navigate(['/votos']);
  }

  onClickCerrar(){
    this.ServiceSecurity.cerrarSesion();
  }

}
