export class Voto {
    nombres: string;
    apellidos: string;
    profesion: string;
    lugarTrabajo: string;
    edad: number;
    valorVoto: string;
}