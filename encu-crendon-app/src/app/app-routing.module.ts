import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { VotoComponent } from './_pages/voto/voto.component';
import { VotosComponent } from './_pages/votos/votos.component';
import { SecurityComponent } from './_pages/security/security.component';
import { InicioComponent } from './_pages/inicio/inicio.component';
import { AuthGuard } from './_shared/guard/auth.guard';

const rutasNavegacion : Routes = [
    { path: 'inicio', component: InicioComponent,canActivate: [AuthGuard] },
    { path: 'security', component: SecurityComponent },
    { path: 'voto', component: VotoComponent,canActivate: [AuthGuard] },
    { path: 'votos', component: VotosComponent, canActivate: [AuthGuard] },
    { path: '**', component: AppComponent, pathMatch: 'full'}
];

@NgModule({
    imports : [RouterModule.forRoot(rutasNavegacion)],
    exports : [RouterModule]
})
export class AppRoutingModule{} 