import { Component, OnInit } from '@angular/core';
import { SecurityService } from './_service/security.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit  {
  title = 'Encuesta - CRendon';

  constructor(
    private ServiceSecurity: SecurityService) {

  }

  ngOnInit() {
    this.ServiceSecurity.inicioSesion();
  }
}


