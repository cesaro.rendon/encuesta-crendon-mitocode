import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Voto } from '../_model/Voto';
import { HOST_BACKEND } from '../_shared/constants';

@Injectable({
  providedIn: 'root'
})

export class ConsultarVotosService {

  URL_MARCADOR= `${HOST_BACKEND}/api/voto/todos`;

  constructor(private http:HttpClient) {}

  consultarAllVotos(){
    return this.http.get<Array<Voto>>(this.URL_MARCADOR);
  }
}
