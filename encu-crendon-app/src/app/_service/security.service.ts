import { Injectable } from '@angular/core';
import { HOST_BACKEND, PARAM_USUARIO, ACCESS_TOKEN_NAME, REFRESH_TOKEN_NAME, TOKEN_NAME } from '../_shared/constants';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { LoginDTO } from '../_model/LoginDTO';
import { BasicAccess } from '../_model/BasicAccess';

@Injectable({
  providedIn: 'root',
})
export class SecurityService {

  urlOauth: string = `${HOST_BACKEND}/api/security/token`;
  urlLogin: string = `${HOST_BACKEND}/api/security/login`;
  urlRefreshToken: string = `${HOST_BACKEND}/api/security/refresh-token`;
  urlSignOut: string = `${HOST_BACKEND}/api/security/signout`;

  constructor(
    private http: HttpClient,
    private router: Router) { }

  login(loginDTO: LoginDTO) {
    return this.http.post(`${this.urlLogin}`, loginDTO);
  }

  validarToken() {
    return this.http.post(this.urlOauth, "");
  }

  refreshToken() {
    let request = new BasicAccess();
    request.token = sessionStorage.getItem(TOKEN_NAME);
    //request.token = sessionStorage.getItem(REFRESH_TOKEN_NAME);
    return this.http.post(this.urlRefreshToken, request);
  }

  cerrarSesion() {
    let request = new BasicAccess();
    request.token = sessionStorage.getItem(TOKEN_NAME);
    //request.token = sessionStorage.getItem(ACCESS_TOKEN_NAME);
    /*
    this.http.post(this.urlSignOut, request).subscribe((data:any)=>{
      console.log(data.body);
    }, (error)=>{
      console.log(error);
    });
    */
    sessionStorage.clear();
    console.log('Se borro tokens de storage');
    setTimeout(() => {
      window.location.href = 'https://encuesta-crendon-mitocode.auth.us-east-1.amazoncognito.com/login?response_type=token&client_id=1brn6kve37rl4gj6c7je4t721q&redirect_uri=https://d1waxv2fn3nvhb.cloudfront.net/security';
    }, 500);
  }

  esRoleAdmin() {
    let usuario = JSON.parse(sessionStorage.getItem(PARAM_USUARIO));
    let rpta = false;
    if (usuario != null && usuario.authorities !== null) {
      usuario.authorities.forEach(element => {
        console.log(element);
        if (element.authority == "ROLE_ADMIN" || element.authority == "ROLE_ADMINISTRADOR") {
          rpta = true;
        }
      });
    } else {
      this.cerrarSesion();
    }
    return rpta;
  }

  inicioSesion() {
    let usuario = JSON.parse(sessionStorage.getItem(PARAM_USUARIO));
    let rpta = false;

    if (usuario != null && usuario.authorities != null) {
      return rpta = true;
    } else {
      return false;
    }

  }
}
