import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Voto } from '../_model/Voto';
import { HttpHeaders } from '@angular/common/http';
import { HOST_BACKEND } from '../_shared/constants';

@Injectable({
  providedIn: 'root'
})

export class EnviarVotoService {

  URL_MARCADOR=`${HOST_BACKEND}/api/voto/registrar`;
  
  constructor(private http:HttpClient) {}

  enviarVoto(envio:Voto){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post(this.URL_MARCADOR,JSON.stringify(envio),httpOptions,);    
  }
}
