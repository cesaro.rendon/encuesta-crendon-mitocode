package co.com.crendon.service.imp;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.crendon.dao.IVotoDao;
import co.com.crendon.dto.VotoDTO;
import co.com.crendon.model.Voto;
import co.com.crendon.service.IVotoService;


@Service
public class VotoServiceImpl implements IVotoService {

	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private IVotoDao votoDao;

	@Override
	public List<Voto> obtenerVotos() {		
		return votoDao.findAll();
	}

	@Override
	public void registrarVoto(VotoDTO voto) {		
		Voto vo = new Voto();
		vo.setNombres(voto.getNombres());
		vo.setApellidos(voto.getApellidos());
		vo.setProfesion(voto.getProfesion());
		vo.setLugarTrabajo(voto.getLugarTrabajo());
		vo.setEdad(voto.getEdad());
		vo.setValorVoto(voto.getValorVoto());
		votoDao.save(vo);
	}

	
}
