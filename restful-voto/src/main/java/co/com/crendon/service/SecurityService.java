package co.com.crendon.service;

import co.com.crendon.dto.RespuestaApi;

public interface SecurityService {

	public RespuestaApi getToken(String username, String password);
	public RespuestaApi signOut(String token);
	public RespuestaApi refreshToken(String token);
}
