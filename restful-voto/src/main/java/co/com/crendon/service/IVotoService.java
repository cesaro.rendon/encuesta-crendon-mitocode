package co.com.crendon.service;

import java.util.List;

import co.com.crendon.dto.VotoDTO;
import co.com.crendon.model.Voto;

public interface IVotoService {

	public List<Voto> obtenerVotos();
	public void registrarVoto(VotoDTO voto);
}
