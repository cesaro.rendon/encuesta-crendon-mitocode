package co.com.crendon.service.imp;

import java.util.HashMap; 
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import co.com.crendon.dto.RespuestaApi;
import co.com.crendon.service.SecurityService;


@Service
public class SecurityServiceImpl implements SecurityService {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public RespuestaApi getToken(String username, String password) {
		RespuestaApi rpta = new RespuestaApi();
		rpta.setStatus("ERROR");
		rpta.setBody("No se pudo autenticar");

		Map<String, String> authParams = new HashMap<String, String>();
		authParams.put("USERNAME", username);
		authParams.put("PASSWORD", password);

		try {
			rpta.setStatus("OK");
			rpta.setIdToken("TOKEN-PRUEBA");
			rpta.setRefreshToken("TOKEN-PRUEBA");
		} catch(Exception e) {
			rpta.setBody("Reinicie su password");
			rpta.setStatus("OK-RESET");
		}

		return rpta;
	}

	
	/**
	 * Se puede ingresar el accessToken o idToken
	 */
	@Override
	public RespuestaApi signOut(String token) {
		RespuestaApi rpta = new RespuestaApi();
		rpta.setStatus("ERROR");
		rpta.setBody("No se pudo autenticar");
		
		try {
			rpta.setStatus("OK");
			rpta.setBody("SignOut correcto");
		}catch(Exception e) {
			logger.error("[signOut] Ocurrio un error inesperado: ", e);
			rpta.setBody(e.getMessage());
		}
		
		return rpta;
	}

	/**
	 * Se necesita el refreshToken como input
	 */
	@Override
	public RespuestaApi refreshToken(String token) {
		RespuestaApi rpta = new RespuestaApi();
		rpta.setStatus("ERROR");
		rpta.setBody("No se pudo autenticar");

		Map<String, String> authParams = new HashMap<String, String>();
		authParams.put("REFRESH_TOKEN", token);

		try {
			
		} catch(Exception e) {
			rpta.setBody("Reinicie su password");
			rpta.setStatus("OK-RESET");
		}

		return rpta;
	}
}
