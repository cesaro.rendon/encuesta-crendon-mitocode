package co.com.crendon.controller.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.crendon.dto.VotoDTO;
import co.com.crendon.model.Voto;
import co.com.crendon.service.IVotoService;

@RestController
@CrossOrigin(origins="*")

@RequestMapping("api/voto")
public class VotoApi {

	public static final Logger logger = LoggerFactory.getLogger(VotoApi.class);

	@Autowired
	private IVotoService serviceVoto;

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping(value="todos", produces = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<List<Voto>> obtenerVotos(){
		try {
			return new ResponseEntity<List<Voto>>(serviceVoto.obtenerVotos(), HttpStatus.OK);

		} catch (Exception e) {
			logger.error("Error: ",e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	
	@PostMapping(value = "registrar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> registrarVoto(@RequestBody VotoDTO voto) {
		logger.info("Creating voto : {}", voto.getNombres());
		
		serviceVoto.registrarVoto(voto);
		

		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}
}
