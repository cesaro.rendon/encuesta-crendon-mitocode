package co.com.crendon.dto;


public class VotoDTO {
	
	private String nombres;
	private String apellidos;
	private String profesion;
	private String lugarTrabajo;
	private Integer edad;
	private String valorVoto;
	
	
	public VotoDTO() {
		super();
	}
	
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	public String getProfesion() {
		return profesion;
	}
	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}
	public String getLugarTrabajo() {
		return lugarTrabajo;
	}
	public void setLugarTrabajo(String lugarTrabajo) {
		this.lugarTrabajo = lugarTrabajo;
	}

	public String getValorVoto() {
		return valorVoto;
	}
	public void setValorVoto(String valorVoto) {
		this.valorVoto = valorVoto;
	}
	
}
