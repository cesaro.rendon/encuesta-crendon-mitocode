package co.com.crendon.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.crendon.model.Voto;

@Repository
public interface IVotoDao extends JpaRepository<Voto, Integer> {

}
